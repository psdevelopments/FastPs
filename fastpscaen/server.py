# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

try:
    from tango import DevFailed, Util
except Exception:
    from PyTango import DevFailed, Util
import sys
import traceback

from .device import FastPSClass, FastPSDevice

__author__ = "Roger Pastor Ortiz/Sergi Blanch-Torne"
__maintainer__ = "Emilio Morales"
__copyright__ = "Copyright 2015-2024, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

SERVER_NAME = "FastPS"


def run(args=None):
    __version__ = "3.1.0"
    print("Device Server Version: {}".format(__version__))

    try:
        if not args:
            args = sys.argv[1:]
            args = [SERVER_NAME] + list(args)
        util = Util(args)
        util.add_class(FastPSClass, FastPSDevice)
        u = Util.instance()
        u.server_init()
        u.server_run()

    except DevFailed as e:
        print("-------> Received a DevFailed exception: {}".format(e))
        print(traceback.format_exc())
    except Exception as e:
        print("-------> An unforeseen exception occurred: {}".format(e))
        print(traceback.format_exc())


if __name__ == "__main__":
    run()

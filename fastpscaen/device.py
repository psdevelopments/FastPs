# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; If not, see <http://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

try:
    import tango

    DEVICE_IMP = tango.LatestDeviceImpl
    tango.Device_4Impl
except Exception:
    import PyTango as tango

    DEVICE_IMP = tango.Device_4Impl
import pprint
import threading
import traceback
from math import isnan
from time import time

from .core import FastPS, StatusMode

__author__ = "Roger Pastor Ortiz/Sergi Blanch-Torne"
__maintainer__ = "Emilio Morales"
__copyright__ = "Copyright 2015-2018, CELLS / ALBA Synchrotron"
__license__ = "GPLv3+"

DEV_STATE_UNKNOWN = tango.DevState.UNKNOWN
DEV_STATE_FAULT = tango.DevState.FAULT
DEV_STATE_ON = tango.DevState.ON
DEV_STATE_OFF = tango.DevState.OFF
DEV_STATE_INIT = tango.DevState.INIT

ATTR_QUALITY_INVALID = tango.AttrQuality.ATTR_INVALID
ATTR_QUALITY_VALID = tango.AttrQuality.ATTR_VALID
ATTR_QUALITY_CHANGING = tango.AttrQuality.ATTR_CHANGING
ATTR_QUALITY_WARNING = tango.AttrQuality.ATTR_WARNING
ATTR_QUALITY_ALARM = tango.AttrQuality.ATTR_ALARM

READ_REQUEST = tango.AttReqType.READ_REQ
WRITE_REQUEST = tango.AttReqType.WRITE_REQ


def ExceptionHandler(func):
    """
    Class to decorate the exception management
    :param func: function
    """

    def new_func(*args, **kwargs):
        """
        Method to execute function an input an entry on debug stream and
        warning in case of failure
        :param kwargs: function params
        :return: value: if it is a possible one
        """
        try:
            obj = args[0]
            obj.debug_stream(
                "Entering in {} ({})".format(func.__name__, repr(args))
            )
            result = func(*args, **kwargs)
            obj.debug_stream("Exiting {}".format(func.__name__))
            return result
        except Exception as e:
            obj.warn_stream(
                "Hardware warning in {}: {}".format(func.__name__, e)
            )
            traceback.print_exc()
            raise e

    return new_func


class FastPSClass(tango.DeviceClass):
    """
    Fast PS class
    """

    # Class Properties
    class_property_list = {}

    # Device Properties
    device_property_list = {
        "HostIP": [tango.DevString, "FastPS IP", ""],
        "Port": [tango.DevLong, "FastPS Port", 10001],
        "Timeout": [tango.DevLong, "Serial port timeout", 1],
        "Logfile": [
            tango.DevString,
            "FastPS logfile path",
            "/tmp/DS/fast-ps.log",
        ],
        "Model": [
            tango.DevString,
            ("FastPS model, allowed: 'FastPS' or " "'FastPS1K5'"),
            "FastPS",
        ],
    }

    # Command definitions
    cmd_list = {
        "ResetInterlocks": [[tango.DevVoid, "None"], [tango.DevVoid, "None"]],
        "On": [[tango.DevVoid, "None"], [tango.DevVoid, "None"]],
        "Off": [[tango.DevVoid, "None"], [tango.DevVoid, "None"]],
        "Exec": [
            [tango.DevString, "statement to executed"],
            [tango.DevString, "result"],
            {
                "Display level": tango.DispLevel.EXPERT,
            },
        ],
    }

    # Attributes list
    attr_list = {
        "Loop": [
            [
                tango.ArgType.DevString,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ_WRITE,
            ]
        ],
        "Current": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ,
            ],
            {"format": "%6.6f"},
        ],
        "Voltage": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ,
            ],
            {"format": "%6.6f"},
        ],
        "CurrentSetpoint": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ_WRITE,
            ],
            {"format": "%6.6f", "Memorized": "true"},
        ],
        "VoltageSetpoint": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ_WRITE,
            ],
            {"format": "%6.6f", "Memorized": "true"},
        ],
        "CurrentRamp": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ_WRITE,
            ],
            {"format": "%6.6f", "Memorized": "true"},
        ],
        "VoltageRamp": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ_WRITE,
            ],
            {"format": "%6.6f", "Memorized": "true"},
        ],
        "Power": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ,
            ]
        ],
        "LeakageCurrent": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ,
            ],
            {"format": "%6.6f"},
        ],
        "RemoteMode": [
            [
                tango.ArgType.DevBoolean,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ,
            ]
        ],
        "Temperature": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ,
            ]
        ],
        "FirmwareVersion": [
            [
                tango.ArgType.DevString,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ,
            ]
        ],
        "ModuleID": [
            [
                tango.ArgType.DevString,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ,
            ]
        ],
        "Password": [
            [
                tango.ArgType.DevString,
                tango.AttrDataFormat.SCALAR,
                tango.AttrWriteType.READ_WRITE,
            ]
        ],
        "PIDVoltageI": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ_WRITE,
                3,
            ]
        ],
        "PIDVoltageV": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ_WRITE,
                3,
            ]
        ],
        "PIDCurrentI": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ_WRITE,
                3,
            ]
        ],
        "PIDCurrentV": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ_WRITE,
                3,
            ]
        ],
        "CurrentCalib": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ,
                4,
            ]
        ],
        "VoltageCalib": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ,
                4,
            ]
        ],
        "DCLinkCalib": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ,
                2,
            ]
        ],
        "ACLinkCalib": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ,
                2,
            ]
        ],
        "CurrentLeakageCalib": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ,
                2,
            ]
        ],
        "PIDILimits": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ_WRITE,
                2,
            ]
        ],
        "PIDVLimits": [
            [
                tango.ArgType.DevDouble,
                tango.AttrDataFormat.SPECTRUM,
                tango.AttrWriteType.READ_WRITE,
                2,
            ]
        ],
    }

    def __init__(self, name):
        """
        Method to initialise a device as FastPS
        :return:
        """
        tango.DeviceClass.__init__(self, name)
        self.set_type("FastPSDevice")


class FastPSDevice(DEVICE_IMP):
    # ------------------------------------------------------------------
    #   Device constructor
    # ------------------------------------------------------------------
    def __init__(self, cl, name):
        """
        Method to decorate FastPS device object instantiation and to set the
        state machine
        :param cl:
        :param name:
        """
        DEVICE_IMP.__init__(self, cl, name)
        self.info_stream("In FastPSDevice.__init__")
        try:
            self._async = None
            self.fastps = None
            self.newStatus = ""
            self._importantMsgs = []
            self.flags = ""
            self.init_device()
            self._ACLinkCalib = [None] * 2
            self._CurrentCalib = [None] * 4
            self._CurrentLeakageCalib = [None] * 2
            self._DCLinkCalib = [None] * 2
            self._PIDCurrentI = [None] * 3
            self._PIDCurrentV = [None] * 3
            self._PIDILimits = [None] * 2
            self._PIDVoltageI = [None] * 3
            self._PIDVoltageV = [None] * 3
            self._PIDVLimits = [None] * 2
            self._VoltageCalib = [None] * 4

        except Exception as e:
            self.change_state(DEV_STATE_FAULT)
            self.error_stream("Hardware error in __init__(): {}".format(e))
            traceback.print_exc()
            return
        self.state_machine()

    # ------------------------------------------------------------------
    #   Device destructor
    # ------------------------------------------------------------------
    def delete_device(self):
        """
        Method to destroy a FastPS device object.
        """
        # self.fastps.__del__()
        self.fastps = None  # del self.fastps
        self.info_stream("In {}::delete_device()".format(self.get_name()))

    # ------------------------------------------------------------------
    #   Device initialization
    # ------------------------------------------------------------------
    def init_device(self):
        """
        Method to initiate a FastPS object.
        """
        self.info_stream("In {}::init_device()".format(self.get_name()))
        # Gets the properties to init the device instance
        self.get_device_properties(self.get_device_class())
        self.info_stream(
            "Constructor arguments: "
            "HostIp={}, Timeout={}, Logfile={}".format(
                self.HostIP, self.Timeout, self.Logfile
            )
        )
        self.set_change_event("State", True, False)
        self.set_change_event("Status", True, False)
        self.change_state(DEV_STATE_INIT)
        self.change_status("Initializing...")
        self._locals = {"self": self}
        self._globals = globals()
        self._importantMsgs = []
        try:
            if self.fastps is not None:
                ip = self.fastps.ip
                timeout = self.fastps.timeout
                logfile = self.fastps.logfile
                if (
                    ip == self.HostIP
                    and timeout == self.Timeout
                    and logfile == self.Logfile
                ):
                    self.info_stream("Object already exist")
                else:
                    self.warn_stream(
                        "Communications description has change, rebuild it"
                    )
                    self.fastps = None
            if self.fastps is None:
                self.fastps = FastPS(
                    ip=self.HostIP,
                    port=self.Port,
                    name=self.get_name(),
                    timeout=self.Timeout,
                    logfile=self.Logfile,
                    model=self.Model,
                )
                self.fastps.addRvalueChangeCB(self.value_change)
                self.info_stream("Build fresh communications object")
        except SystemError as e:
            self.error_stream("Exception: {}".format(e))
            self.error_stream(traceback.format_exc())
        # Initialize attributes
        self._async = False

        # Set a flag to indicate that the server fires change events manually
        # without the polling to be started for the attribute
        # Define events on attributes
        self.set_change_event("Loop", True, False)
        self.set_change_event("Current", True, False)
        self.set_change_event("Voltage", True, False)
        self.set_change_event("CurrentSetpoint", True, False)
        self.set_change_event("VoltageSetpoint", True, False)
        self.set_change_event("CurrentRamp", True, False)
        self.set_change_event("VoltageRamp", True, False)
        self.set_change_event("Power", True, False)
        self.set_change_event("LeakageCurrent", True, False)
        self.set_change_event("RemoteMode", True, False)
        self.set_change_event("Temperature", True, False)
        self.set_change_event("FirmwareVersion", True, False)
        self.set_change_event("ModuleID", True, False)
        self.set_change_event("Password", True, False)

        self.set_change_event("PIDVoltageI", True, False)
        self.set_change_event("PIDVoltageV", True, False)
        self.set_change_event("PIDCurrentI", True, False)
        self.set_change_event("PIDCurrentV", True, False)
        self.set_change_event("CurrentCalib", True, False)
        self.set_change_event("VoltageCalib", True, False)
        self.set_change_event("DCLinkCalib", True, False)
        self.set_change_event("ACLinkCalib", True, False)
        self.set_change_event("CurrentLeakageCalib", True, False)
        self.set_change_event("PIDILimits", True, False)
        self.set_change_event("PIDVLimits", True, False)

        self.dyn_attr()

        if not self.fastps.isConnected():
            self.addStatusMessage("Communication Problems", important=True)
            self.change_state(DEV_STATE_FAULT)
        else:
            self.change_state(DEV_STATE_OFF)

    # ------------------------------------------------------------------
    #   Create dynamic attributes
    # ------------------------------------------------------------------

    def dyn_attr(self):
        """
        Method to create attributes dynamically if defined on the FastPS
        class list of dynamic attributes.
        """

        exclude_parameters = [
            "current_calibration_param_a",
            "current_calibration_param_b",
            "current_calibration_param_c",
            "current_calibration_param_d",
            "voltage_calibration_param_a",
            "voltage_calibration_param_b",
            "voltage_calibration_param_c",
            "voltage_calibration_param_d",
            "dc_link_calib_a",
            "dc_link_calib_b",
            "ac_link_calib_a",
            "ac_link_calib_b",
            "current_leakage_calib_a",
            "current_leakage_calib_b",
            "pid_i_kp_v",
            "pid_i_ki_v",
            "pid_i_kd_v",
            "pid_i_kp_i",
            "pid_i_ki_i",
            "pid_i_kd_i",
            "pid_i_upper_lim_acc_v",
            "pid_i_lower_lim_acc_v",
            "pid_v_kp_i",
            "pid_v_ki_i",
            "pid_v_kd_i",
            "pid_v_kp_v",
            "pid_v_ki_v",
            "pid_v_kd_v",
            "pid_v_upper_lim_acc_i",
            "pid_v_lower_lim_acc_i",
        ]

        for attr_name in self.fastps.get_parameter_names():
            # TODO set limits and configure
            # If W/R
            if attr_name in exclude_parameters:
                continue

            if self.fastps.is_parameter_writable(attr_name):
                attr_mode_type = tango.AttrWriteType.READ_WRITE
                attr_write_parameter = self.write_Parameters
            # If R only
            else:
                attr_mode_type = tango.AttrWriteType.READ
                attr_write_parameter = None

            attr_data = tango.Attr(
                attr_name, tango.ArgType.DevString, attr_mode_type
            )

            self.add_attribute(
                attr_data,
                self.read_Parameters,
                attr_write_parameter,
                self.is_Parameter_allowed,
            )
            self.set_change_event(attr_name, True, False)

    # ------------------------------------------------------------------
    #   State machine implementation
    # ------------------------------------------------------------------
    @ExceptionHandler
    def state_machine(self):
        """
        Method to manage the state machine
        :return: None
        """
        if self._async:
            return
        # Update the status obj
        if self.fastps is None:
            self.error_stream("FastPS not initialized")
            return
        if not self.fastps.isConnected():
            self.fastps.connect()
        try:
            internalState = self.fastps.state
        except Exception:
            internalState = StatusMode.FAULT
        # Set device status depending on HW status
        if internalState == StatusMode.ON:
            state = DEV_STATE_ON
            # status = 'Device output ON'
        elif internalState == StatusMode.OFF:
            state = DEV_STATE_OFF
            # status = 'Device output OFF'
        elif internalState == StatusMode.FAULT:
            state = DEV_STATE_FAULT
            try:
                self.flags = self.fastps._status.flags()
            except Exception:
                self.error_stream("Couldn't resolve fault flags")
                self.error_stream(traceback.format_exc())
                if not self.fastps.isConnected():
                    self.addStatusMessage(
                        "Communication Problems", important=True
                    )
        else:
            state = DEV_STATE_UNKNOWN
            # status = 'Unknown HW status'
        self.change_state(state)
        # self.change_status(status)

    def change_state(self, newState):
        oldState = self.get_state()
        if newState != oldState:
            self.debug_stream(
                "state change from {} to {}".format(oldState, newState)
            )
            self.set_state(newState)
            self.push_change_event("State", self.get_state())
            # clean important messages once state changes

            if newState in (
                DEV_STATE_FAULT,
                DEV_STATE_INIT,
            ):
                if self.flags:
                    self.change_status(self.addStatusMessage(self.flags))
                else:
                    self.change_status(
                        self.addStatusMessage("Communication Problems")
                    )
            else:
                self.change_status(self._rebuildStatusMessage())

    def addStatusMessage(self, msg, important=False):
        if not self._importantMsgs:
            newStatus = "{} due to {}".format(
                self._rebuildStatusMessage(), msg
            )
        else:
            newStatus = "{}".format(self._rebuildStatusMessage())

        if important and msg not in self._importantMsgs:
            self._importantMsgs.append(msg)

        return newStatus

    def _rebuildStatusMessage(self):
        state = self.get_state()
        if state in (DEV_STATE_OFF, DEV_STATE_ON):
            newStatus = "Power {}".format(str(state))
        elif state in (DEV_STATE_FAULT,):
            newStatus = "Device in {}".format(str(state).upper())
        else:
            newStatus = "Device is {} state".format(str(state))
        if self._importantMsgs:
            newStatus = "Device in {} state due to:".format(str(state))
            newStatus += "\n" + "\n".join(each for each in self._importantMsgs)
        return newStatus

    def change_status(self, newStatus):
        oldStatus = self.get_status()
        if newStatus != oldStatus:
            self.debug_stream(
                "status change from {} to {}".format(oldStatus, newStatus)
            )
            self.set_status(newStatus)
            self.push_change_event("Status", self.get_status())

    def value_change(self, name, value, timestamp, quality):
        try:
            attrName = {
                "current_setpoint": "Currentsetpoint",
                "voltage_setpoint": "VoltageSetpoint",
                "current_ramp": "CurrentRamp",
                "voltage_ramp": "VoltageRamp",
                "leakage_current": "LeakageCurrent",
                "remote_mode": "RemoteMode",
            }.get(name, name)
            spectrumAttr = {
                "ac_link_calib_a": ["ACLinkCalib", 0],
                "ac_link_calib_b": ["ACLinkCalib", 1],
                "current_calibration_param_a": ["CurrentCalib", 0],
                "current_calibration_param_b": ["CurrentCalib", 1],
                "current_calibration_param_c": ["CurrentCalib", 2],
                "current_calibration_param_d": ["CurrentCalib", 3],
                "current_leakage_calib_a": ["CurrentLeakageCalib", 0],
                "current_leakage_calib_b": ["CurrentLeakageCalib", 1],
                "dc_link_calib_a": ["DCLinkCalib", 0],
                "dc_link_calib_b": ["DCLinkCalib", 1],
                "pid_i_kd_i": ["PIDCurrentI", 2],
                "pid_i_kd_v": ["PIDCurrentV", 2],
                "pid_i_ki_i": ["PIDCurrentI", 1],
                "pid_i_ki_v": ["PIDCurrentV", 1],
                "pid_i_kp_i": ["PIDCurrentI", 0],
                "pid_i_kp_v": ["PIDCurrentV", 0],
                "pid_i_lower_lim_acc_v": ["PIDILimits", 0],
                "pid_i_upper_lim_acc_v": ["PIDILimits", 1],
                "pid_v_kd_i": ["PIDVoltageI", 2],
                "pid_v_kd_v": ["PIDVoltageV", 2],
                "pid_v_ki_i": ["PIDVoltageI", 1],
                "pid_v_ki_v": ["PIDVoltageV", 1],
                "pid_v_kp_i": ["PIDVoltageI", 0],
                "pid_v_kp_v": ["PIDVoltageV", 0],
                "pid_v_lower_lim_acc_i": ["PIDVLimits", 0],
                "pid_v_upper_lim_acc_i": ["PIDVLimits", 1],
                "voltage_calibration_param_a": ["VoltageCalib", 0],
                "voltage_calibration_param_b": ["VoltageCalib", 1],
                "voltage_calibration_param_c": ["VoltageCalib", 2],
                "voltage_calibration_param_d": ["VoltageCalib", 3],
            }.get(name, None)
            if spectrumAttr is not None:
                attrName = spectrumAttr[0]
                position = spectrumAttr[1]
                lst = getattr(self, "_{}".format(attrName))
                lst[position] = float(value)
                value = lst
                if value.count(None) > 0:
                    self.debug_stream(
                        "Incomplete array for {}".format(attrName)
                    )
                    return
                self.push_change_event(
                    attrName, value, timestamp, quality, len(value)
                )
            else:
                self.push_change_event(attrName, value, timestamp, quality)
            self.debug_stream(
                "Attribute {} value change {}, {}, {}".format(
                    attrName, value, timestamp, quality
                )
            )
        except Exception:
            self.error_stream(
                "CANNOT Attribute {} value change {}".format(name, value)
            )

    # ------------------------------------------------------------------
    #   Always executed hook method
    # ------------------------------------------------------------------
    def always_executed_hook(self):
        """
        Tango method to execute always before any method that logs stream
        info, updates the state machine and the State and Status
        :return: None
        """
        self.info_stream(
            "In {}::always_executed_hook()".format(self.get_name())
        )
        self.state_machine()  # FIXME: this must be an internal loop processing

    # ------------------------------------------------------------------
    #   ATTRIBUTES
    # ------------------------------------------------------------------
    def read_attr_hardware(self, data):
        pass

    # ------------------------------------------------------------------
    #   read & write PIDVoltageI
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_PIDVoltageI(self, the_att):
        """
        Method to read the PID voltage mode Current bits kp, ki and kd and
        update their value.
        :param the_att: actual name of the attribute
        :return: None
        """
        kp = float(self.fastps.pid_v_kp_i)
        ki = float(self.fastps.pid_v_ki_i)
        kd = float(self.fastps.pid_v_kd_i)
        self._PIDVoltageI = [kp, ki, kd]
        the_att.set_value(self._PIDVoltageI)

    @ExceptionHandler
    def write_PIDVoltageI(self, the_att):
        """
        Method to set the PID voltage mode Current bits kp, ki and kd and
        trigger change event.
        :param the_att: actual name of the attribute
        :return: None
        """
        data = []
        the_att.get_write_value(data)
        kp, ki, kd = data
        self.fastps.pid_v_kp_i = kp
        self.fastps.pid_v_ki_i = ki
        self.fastps.pid_v_kd_i = kd
        self.push_change_event("PIDVoltageI", data)

    def is_PIDVoltageI_allowed(self, req_type):
        """
        Method to check if the PID voltage mode Current bits are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read & write PIDVoltageV
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_PIDVoltageV(self, the_att):
        """
        Method to read the PID voltage mode Voltage bits kp, ki and kd and
        update their value.
        :param the_att: actual name of the attribute
        :return: None
        """
        kp = float(self.fastps.pid_v_kp_v)
        ki = float(self.fastps.pid_v_ki_v)
        kd = float(self.fastps.pid_v_kd_v)
        self._PIDVoltageV = [kp, ki, kd]
        the_att.set_value(self._PIDVoltageV)

    @ExceptionHandler
    def write_PIDVoltageV(self, the_att):
        """
        Method to set the PID voltage mode Voltage bits kp, ki and kd and
        trigger change event.
        :param the_att: actual name of the attribute
        :return: None
        """
        data = []
        the_att.get_write_value(data)
        kp, ki, kd = data
        self.fastps.pid_v_kp_v = kp
        self.fastps.pid_v_ki_v = ki
        self.fastps.pid_v_kd_v = kd
        self.push_change_event("PIDVoltageV", data)

    def is_PIDVoltageV_allowed(self, req_type):
        """
        Method to check if the PID voltage mode Voltage bits are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read & write PIDCurrentI
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_PIDCurrentI(self, the_att):
        """
        Method to read the PID current mode Current bits kp, ki and kd and
        update their value.
        :param the_att: actual name of the attribute
        :return: None
        """
        kp = float(self.fastps.pid_i_kp_i)
        ki = float(self.fastps.pid_i_ki_i)
        kd = float(self.fastps.pid_i_kd_i)
        self._PIDCurrentI = [kp, ki, kd]
        the_att.set_value(self._PIDCurrentI)

    @ExceptionHandler
    def write_PIDCurrentI(self, the_att):
        """
        Method to set the PID current mode Current bits kp, ki and kd and
        trigger change event.
        :param the_att: actual name of the attribute
        :return: None
        """
        data = []
        the_att.get_write_value(data)
        kp, ki, kd = data
        self.fastps.pid_i_kp_i = kp
        self.fastps.pid_i_ki_i = ki
        self.fastps.pid_i_kd_i = kd
        self.push_change_event("PIDCurrentI", data)

    def is_PIDCurrentI_allowed(self, req_type):
        """
        Method to check if the PID current mode Current bits are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read & write PIDCurrentV
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_PIDCurrentV(self, the_att):
        """
        Method to read the PID current mode Voltage bits kp, ki and kd and
        update their value.
        :param the_att: actual name of the attribute
        :return: None
        """
        kp = float(self.fastps.pid_i_kp_v)
        ki = float(self.fastps.pid_i_ki_v)
        kd = float(self.fastps.pid_i_kd_v)
        self._PIDCurrentV = [kp, ki, kd]
        the_att.set_value(self._PIDCurrentV)

    @ExceptionHandler
    def write_PIDCurrentV(self, the_att):
        """
        Method to set the PID current mode Voltage bits kp, ki and kd and
        trigger change event.
        :param the_att: actual name of the attribute
        :return: None
        """
        data = []
        the_att.get_write_value(data)
        kp, ki, kd = data
        self.fastps.pid_i_kp_v = kp
        self.fastps.pid_i_ki_v = ki
        self.fastps.pid_i_kd_v = kd
        self.push_change_event("PIDCurrentV", data)

    def is_PIDCurrentV_allowed(self, req_type):
        """
        Method to check if the PID current mode Voltage bits are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read CurrentCalib
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_CurrentCalib(self, the_att):
        """
        Method to read the Current calibration parameters
        :param the_att: actual name of the attribute
        :return: None
        """
        a = float(self.fastps.current_calibration_param_a)
        b = float(self.fastps.current_calibration_param_b)
        c = float(self.fastps.current_calibration_param_c)
        d = float(self.fastps.current_calibration_param_d)
        self._CurrentCalib = [a, b, c, d]
        the_att.set_value(self._CurrentCalib)

    def is_CurrentCalib_allowed(self, req_type):
        """
        Method to check if the Current calibration parameters are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read VoltageCalib
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_VoltageCalib(self, the_att):
        """
        Method to read the Voltage calibration parameters
        :param the_att: actual name of the attribute
        :return: None
        """
        a = float(self.fastps.voltage_calibration_param_a)
        b = float(self.fastps.voltage_calibration_param_b)
        c = float(self.fastps.voltage_calibration_param_c)
        d = float(self.fastps.voltage_calibration_param_d)
        self._VoltageCalib = [a, b, c, d]
        the_att.set_value(self._VoltageCalib)

    def is_VoltageCalib_allowed(self, req_type):
        """
        Method to check if the Voltage calibration parameters are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read DCLinkCalib
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_DCLinkCalib(self, the_att):
        """
        Method to read the DC link calibration parameters
        :param the_att: actual name of the attribute
        :return: None
        """
        a = float(self.fastps.dc_link_calib_a)
        b = float(self.fastps.dc_link_calib_b)
        self._DCLinkCalib = [a, b]
        the_att.set_value(self._DCLinkCalib)

    def is_DCLinkCalibr_allowed(self, req_type):
        """
        Method to check if the DC link calibration parameters are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read ACLinkCalib
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_ACLinkCalib(self, the_att):
        """
        Method to read the AC link calibration parameters
        :param the_att: actual name of the attribute
        :return: None
        """
        a = float(self.fastps.ac_link_calib_a)
        b = float(self.fastps.ac_link_calib_b)
        self._ACLinkCalib = [a, b]
        the_att.set_value(self._ACLinkCalib)

    def is_ACLinkCalib_allowed(self, req_type):
        """
        Method to check if the AC link calibration parameters are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read CurrentLeakageCalib
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_CurrentLeakageCalib(self, the_att):
        """
        Method to read the leakage calibration parameters
        :param the_att: actual name of the attribute
        :return: None
        """
        a = float(self.fastps.current_leakage_calib_a)
        b = float(self.fastps.current_leakage_calib_b)
        self._CurrentLeakageCalib = [a, b]
        the_att.set_value(self._CurrentLeakageCalib)

    def is_CurrentLeakageCalib_allowed(self, req_type):
        """
        Method to check if the leakage calibration parameters are available to
        interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read PIDILimits
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_PIDILimits(self, the_att):
        """
        Method to read the PID current mode Voltage limits.
        :param the_att: actual name of the attribute
        :return: None
        """
        a = float(self.fastps.pid_i_upper_lim_acc_v)
        b = float(self.fastps.pid_i_lower_lim_acc_v)
        self._PIDILimits = [a, b]
        the_att.set_value(self._PIDILimits)

    @ExceptionHandler
    def write_PIDILimits(self, the_att):
        """
        Method to set the PID current mode Voltage limits and trigger change
        event.
        :param the_att: actual name of the attribute
        :return: None
        """
        data = []
        the_att.get_write_value(data)
        upper, lower = data
        self.fastps.pid_i_upper_lim_acc_v = upper
        self.fastps.pid_i_lower_lim_acc_v = lower
        self.push_change_event("PIDILimits", data)

    def is_PIDILimits_allowed(self, req_type):
        """
        Method to check if the PID current mode Voltage limits are available
        to interact with.
        :param the_att: actual name of the attribute
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read PIDVLimits
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_PIDVLimits(self, the_att):
        """
        Method to read the PID voltage mode Current limits.
        :param the_att: actual name of the attribute
        :return: None
        """
        a = float(self.fastps.pid_v_upper_lim_acc_i)
        b = float(self.fastps.pid_v_lower_lim_acc_i)
        self._PIDVLimits = [a, b]
        the_att.set_value(self._PIDVLimits)

    @ExceptionHandler
    def write_PIDVLimits(self, the_att):
        """
        Method to set the PID voltage mode Current limits and trigger change
        event.
        :return: None
        """
        data = []
        the_att.get_write_value(data)
        upper, lower = data
        self.fastps.pid_v_upper_lim_acc_i = upper
        self.fastps.pid_v_lower_lim_acc_i = lower
        self.push_change_event("PIDVLimits", data)

    def is_PIDVLimits_allowed(self, req_type):
        """
        Method to check if the PID voltage mode Current limits are available
        to interact with.
        :return: None
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read & write Loop attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_Loop(self, the_att):
        """
        Method to read the loop mode.
        :param the_att: the actual name of the attribute
        :return: None
        """
        the_att.set_value(self.fastps.loop)

    @ExceptionHandler
    def write_Loop(self, the_att):
        """
        Method to set the loop mode and fire change event trigger.
        :return: None
        """
        data = []
        the_att.get_write_value(data)
        self.fastps.loop = data[0]
        self.push_change_event("Loop", data[0])

    def is_Loop_allowed(self, req_type):
        """
        Method to check if loop mode methods are available.
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read Current attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_Current(self, the_att):
        """
        Method to read the actual current value
        :param the_att: the actual name of the attribute
        :return:
        """
        the_att.set_value(self.fastps.current)

    def is_Current_allowed(self, req_type):
        """
        Method to check if current param value is available to read.
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read Voltage attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_Voltage(self, the_att):
        """
        Method to read the actual voltage value
        :param the_att: the actual name of the attribute
        :return:
        """
        the_att.set_value(self.fastps.voltage)

    def is_Voltage_allowed(self, req_type):
        """
        Method to check if voltage param value is available to read.
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read & write CurrentSetpoint attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_CurrentSetpoint(self, the_att):
        """
        Method to read the last applied Current setpoint
        :param the_att: actual name of the attribute on the class
        :return:
        """
        value = self.fastps.current_setpoint
        if isnan(value):
            if self.get_state() is DEV_STATE_OFF:
                value = self.fastps.current_setpoint_wvalue
                the_att.set_value(value)
            else:
                the_att.set_value_date_quality(0, time(), ATTR_QUALITY_INVALID)
        else:
            the_att.set_value(value)

    @ExceptionHandler
    def write_CurrentSetpoint(self, the_att):
        """
        Method to set a Current setpoint on the device
        :param the_att: actual name of the attribute on the class
        :return:
        """
        data = []
        the_att.get_write_value(data)
        if not isnan(data[0]):
            self.fastps.current_setpoint = data[0]
            self.push_change_event(
                "CurrentSetpoint", self.fastps.current_setpoint
            )

    def is_CurrentSetpoint_allowed(self, req_type):
        """
        Method to check if execution of the current setpoint methods is allowed
        :param req_type:
        :return:
        """
        # if req_type is WRITE_REQUEST and \
        #         self.get_state() is (DEV_STATE_OFF, ):
        #     return False
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read & write VoltageSetpoint attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_VoltageSetpoint(self, the_att):
        """
        Method to read the last applied Voltage setpoint
        :param the_att: actual name of the attribute on the class
        :return:
        """
        value = self.fastps.voltage_setpoint
        if isnan(value):
            if self.get_state() is DEV_STATE_OFF:
                value = self.fastps.voltage_setpoint_wvalue
                the_att.set_value(value)
            else:
                the_att.set_value_date_quality(0, time(), ATTR_QUALITY_INVALID)
        else:
            the_att.set_value(value)

    @ExceptionHandler
    def write_VotlageSetpoint(self, the_att):
        """
        Method to set a Voltage setpoint on the device
        :param the_att: actual name of the attribute on the class
        :return:
        """
        data = []
        the_att.get_write_value(data)
        if not isnan(data[0]):
            self.fastps.voltage_setpoint = data[0]
            self.push_change_event("VoltageSetpoint", data[0])

    def is_VoltageSetpoint_allowed(self, req_type):
        """
        Method to check if execution of the voltage setpoint methods is allowed
        :param req_type:
        :return:
        """
        # if req_type is WRITE_REQUEST and \
        #         self.get_state() is (DEV_STATE_OFF, ):
        #     return False
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read & write CurrentRamp attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_CurrentRamp(self, the_att):
        """
        Method to read the last applied Current ramp setpoint
        :param the_att: actual name of the attribute on the class
        :return:
        """
        value = self.fastps.current_ramp
        if isnan(value):
            if self.get_state() is DEV_STATE_OFF:
                value = self.fastps.current_ramp_wvalue
                the_att.set_value(value)
            else:
                the_att.set_value_date_quality(0, time(), ATTR_QUALITY_INVALID)
        else:
            the_att.set_value(value)

    @ExceptionHandler
    def write_CurrentRamp(self, the_att):
        """
        Method to set a Current ramp setpoint on the device
        :param the_att: actual name of the attribute on the class
        :return:
        """
        data = []
        the_att.get_write_value(data)
        if not isnan(data[0]):
            self.fastps.current_ramp = data[0]
            self.push_change_event("CurrentRamp", data[0])

    def is_CurrentRamp_allowed(self, req_type):
        """
        Method to check if execution of the Current ramp setpoint methods is
        allowed.
        :param req_type:
        :return:
        """
        # if req_type is WRITE_REQUEST and \
        #         self.get_state() is (DEV_STATE_OFF, ):
        #     return False
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read & write VoltageRamp attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_VoltageRamp(self, the_att):
        """
        Method to read the last applied Voltage ramp setpoint
        :param the_att: actual name of the attribute on the class
        :return:
        """
        value = self.fastps.voltage_ramp
        if isnan(value):
            if self.get_state() is DEV_STATE_OFF:
                value = self.fastps.voltage_ramp_wvalue
                the_att.set_value(value)
            else:
                the_att.set_value_date_quality(0, time(), ATTR_QUALITY_INVALID)
        else:
            the_att.set_value(value)

    @ExceptionHandler
    def write_VoltageRamp(self, the_att):
        """
        Method to set a Voltage ramp setpoint on the device
        :param the_att: actual name of the attribute on the class
        :return:
        """
        data = []
        the_att.get_write_value(data)
        if not isnan(data[0]):
            self.fastps.voltage_ramp = data[0]
            self.push_change_event("VoltageRamp", data[0])

    def is_VoltageRamp_allowed(self, req_type):
        """
        Method to check if execution of the Voltage ramp setpoint methods is
        allowed.
        :param req_type:
        :return:
        """
        # if req_type is WRITE_REQUEST and \
        #         self.get_state() is (DEV_STATE_OFF, ):
        #     return False
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read Power attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_Power(self, the_att):
        """
        Method to read the power generated by the device
        :param the_att: Name of the attribute on the class
        :return:
        """
        the_att.set_value(self.fastps.power)

    def is_Power_allowed(self, req_type):
        """
        Method to check if the power reading is permitted
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read LeakageCurrent attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_LeakageCurrent(self, the_att):
        """
        Method to read the leakage current of the device
        :param the_att: Name of the attribute on the class
        :return:
        """
        the_att.set_value(self.fastps.leakage_current)

    def is_LeakageCurrent_allowed(self, req_type):
        """
        Method to check if the Leakage Current reading is permitted
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read RemoteMode attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_RemoteMode(self, the_att):
        """
        Method to read the RemoteMode of the power converter
        :param the_att: Name of the attribute on the class
        :return:
        """
        the_att.set_value(
            True if self.fastps._status.controlmode == "REMOTE" else False
        )

    def is_RemoteMode_allowed(self, req_type):
        """
        Method to check if the RemoteMode reading is permitted
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read Temperature attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_Temperature(self, the_att):
        """
        Method to read the Temperature of the device.
        :param the_att: name of the attribute on the device
        :return:
        """
        the_att.set_value(self.fastps.temperature)

    def is_Temperature(self, req_type):
        """
        Method to check if access to the parameter is allowed.
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read FirmwareVersion attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_FirmwareVersion(self, the_att):
        """
        Method to read the Firmware version of the HW associated to the device.
        :param the_att: name of the attribute on the device
        :return:
        """
        the_att.set_value(self.fastps.firmware_vers)

    def is_FirmwareVersion(self, req_type):
        """
        Method to check if access to the parameter is allowed.
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # ------------------------------------------------------------------
    #   read ModuleID attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_ModuleID(self, the_att):
        """
        Method to read the Module ID of the HW associated to the device.
        :param the_att: name of the attribute on the device
        :return:
        """
        the_att.set_value(self.fastps.module_id)

    def is_ModuleID(self, req_type):
        """
        Method to check if access to the parameter is allowed.
        :param req_type:
        :return:
        """
        return self.get_state() in (DEV_STATE_ON,)

    # ------------------------------------------------------------------
    #   read & write Password attribute
    # ------------------------------------------------------------------
    @ExceptionHandler
    def read_Password(self, the_att):
        """
        Method to read the current privileges of the device.
        :param the_att: name of the attribute on the device
        :return:
        """
        the_att.set_value(self.fastps.password)

    @ExceptionHandler
    def write_Password(self, the_att):
        """
        Method to unlock or lock the private parameters of the HW of the
        device.
        :param the_att: name of the attribute on the device
        :return:
        """
        data = []
        the_att.get_write_value(data)
        self.fastps.password = data[0]
        self.push_change_event("Password", data[0])

    def is_Password_allowed(self, req_type):
        """
        Method to check if access to the parameter is allowed.
        :param req_type:
        :return:
        """
        return self.get_state() not in (
            DEV_STATE_FAULT,
            DEV_STATE_INIT,
            DEV_STATE_UNKNOWN,
        )

    # TODO implement thread if timeout greater than 3 sec.

    # ------------------------------------------------------------------
    #   read & write Parameters attribute (dynamic)
    # ------------------------------------------------------------------
    # @ExceptionHandler
    def read_Parameters(self, the_att):
        """
        Method to read a parameter of the device.
        :param the_att: name of the attribute on the device
        :return:
        """
        param_name = the_att.get_name()
        value = self.fastps.__getattr__(param_name)
        the_att.set_value(value)

    # @ExceptionHandler
    def write_Parameters(self, the_att):
        """
        Method to write a value to a parameter of the device.
        :param the_att: name of the attribute on the device
        :return:
        """
        data = []
        attr_name = the_att.get_name()
        the_att.get_write_value(data)
        self.fastps.__setattr__(attr_name, data[0])
        self.push_change_event(attr_name, data[0])

    def is_Parameter_allowed(self, req_type):
        """
        Method to check if accessing the parameter is allowed.
        :param req_type:
        :return:
        """
        return self.get_state() not in (DEV_STATE_INIT, DEV_STATE_UNKNOWN)

    # ------------------------------------------------------------------
    #   COMMANDS
    # ------------------------------------------------------------------
    @ExceptionHandler
    def Off(self):
        """
        Method to Off the output of the HW
        :return:
        """
        self.fastps.off()
        self.change_state(DEV_STATE_OFF)

    def is_Off_allowed(self):
        """
        Method to check if stopping the HW is allowed.
        :return:
        """
        return self.get_state() in (DEV_STATE_ON,)

    @ExceptionHandler
    def On(self):
        """
        Method to enable (On) the output of the HW
        :return:
        """
        self.fastps.on()
        self.change_state(DEV_STATE_ON)

    def is_On_allowed(self):
        """
        Method to check if starting the HW is allowed.
        :return:
        """
        return self.get_state() in (DEV_STATE_OFF,)

    @ExceptionHandler
    def ResetInterlocks(self):
        """
        Method that asynchronously restarts the device and set it to ON state
        :return:
        """
        self._async = True

        t = threading.Thread(target=self._reset)
        t.start()

    def _reset(self):
        """
        Method executed by the thread in charge of restarting the device
        asynchronously and reset the values of the device depending on the
        pre-restart status
        :return:
        """
        original_state = self.get_state()
        self._importantMsgs = []
        self.flags = "Resetting FAST-PS"
        self.change_state(DEV_STATE_INIT)
        self.fastps.reset()

        self.change_state(DEV_STATE_OFF)
        # self.change_status('Device output OFF')

        self.push_change_event("Loop", self.fastps.loop)
        self.push_change_event("FirmwareVersion", self.fastps.firmware_vers)
        self.push_change_event("ModuleID", self.fastps.module_id)
        self.push_change_event("State", self.get_state())
        self.push_change_event("Status", self.get_status())

        self.flags = ""
        if original_state in (DEV_STATE_ON,):
            self.On()

        self._async = False

    def is_ResetInterlocks_allowed(self):
        """
        Method to check if restarting the HW is allowed.
        :return:
        """
        # return self.get_state() in (DEV_STATE_RUNNING, DEV_STATE_FAULT,
        #                             DEV_STATE_UNKNOWN, DEV_STATE_ON)
        return self.get_state() in (
            DEV_STATE_ON,
            DEV_STATE_OFF,
            DEV_STATE_FAULT,
            DEV_STATE_UNKNOWN,
        )

    @ExceptionHandler
    def Exec(self, cmd):
        L = self._locals
        G = self._globals
        try:
            try:
                # interpretation as expression
                result = eval(cmd, G, L)
            except SyntaxError:
                # interpretation as statement
                exec(cmd in G, L)
                result = L.get("y")

        except Exception as exc:
            # handles errors on both eval and exec level
            result = exc

        if isinstance(result, str):
            return result
        elif isinstance(result, BaseException):
            return "{}!\n{}".format(result.__class__.__name__, str(result))
        else:
            return pprint.pformat(result)

FAST-PS CAENels Tango Device Server
==================================

Author(s): Roger Pastor Ortiz, [Sergi Blanch-Torne](sblanch@cells.es)

Maintainer: [Emilio Morales](emorales@cells.es)

![Version](https://img.shields.io/badge/Version-"Version-3.1.0-white"-white.svg)
![Python 3.7](https://img.shields.io/badge/python-python3.7-green.svg)
![Python 3.11](https://img.shields.io/badge/python-python3.11-blue.svg)
![license GPLv3+](https://img.shields.io/badge/license-GPLv3+-green.svg)
![5 - Production](https://img.shields.io/badge/Development_Status-5_--_production-green.svg)

Description: Tango device server to control the FAST-PS of CAENels.
Models supported: FAST-PS, FAST-PS-1K5


Installation instructions:
-------------------------

The setup.cfg file contains an specific config
uration for @ ALBA:

    python3 setup.py install

For a regular installation:

    python3 setup.py install --no-user-cfg

Prepare a test
--------------

From the source files,

    cd test
    python ds_create.py --machine=localhost --simulation --logpath=/tmp

This will generate 16 device servers (what is 1 per sector) with 2 devices each. The script will stay running as it is launching also 32 simulators of the hardware. The last parameter is necessary if the user of the test doesn't have write rights in the usual directory for the logs.

It can be also simply launched the simulator and one device server to work with this simulator:

    import CAENels
    simulator = CAENels.FastPS_PC(port=10002)
    simulator.open()

Device properties necessaries are 'HostIP' and 'Port'. The 'Logfile' property is optional in case one likes to establish an specific output directory for it.

